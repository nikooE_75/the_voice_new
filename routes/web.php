<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('mentors',[\App\Http\Controllers\MentorController::class,"index"]);
Route::get('mentors/{mentor}',[\App\Http\Controllers\MentorController::class,"show"]);
Route::get('activity/{activity}',[\App\Http\Controllers\ActivityController::class,"show"]);
Route::post('mentor/add',[\App\Http\Controllers\MentorController::class,"store"]);
Route::post('team/add',[\App\Http\Controllers\TeamController::class,"store"]);
Route::post('candidate/add',[\App\Http\Controllers\CandidateController::class,"store"]);
Route::post('activity/add',[\App\Http\Controllers\ActivityController::class,"store"]);

