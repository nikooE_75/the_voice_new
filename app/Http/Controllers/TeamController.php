<?php

namespace App\Http\Controllers;

use App\Models\Team;
use Illuminate\Http\Request;

class TeamController extends Controller
{
    public function store(Request $request){
        // return $request->all();
        $team=new Team;
        $team->name =$request->name;
        $team->mentor_id=$request->mentor;
        $team->save();
        return redirect('/mentors');

    }
}
