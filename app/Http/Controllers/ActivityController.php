<?php

namespace App\Http\Controllers;

use App\Models\Activity;
use Illuminate\Http\Request;

class ActivityController extends Controller
{
    public function show(Activity $activity){
        // $mentor = Mentor::find($id);

        return view('activities/show',compact('activity'));

    }
    public function store(Request $request){
        // return $request->all();
        $activity=new Activity;
        $activity->song_name =$request->name;
        $activity->candidate_id=$request->candidate;
        $activity->date=$request->date;
//        $activity->average=0;
        $activity->save();
        return redirect('/mentors');

    }
}
