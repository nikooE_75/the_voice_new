<?php

namespace App\Http\Controllers;

use App\Models\Mentor;
use Illuminate\Http\Request;

class MentorController extends Controller
{
    public function index(){
        $mentors=Mentor::with("teams")->paginate(10);
        return view('mentors/index', compact('mentors'));
    }
    public function show(Mentor $mentor){
       // $mentor = Mentor::find($id);

        return view('mentors/show',compact('mentor'));

    }
    public function store(Request $request){
        // return $request->all();
        $mentor=new Mentor;
        $mentor->name =$request->name;
        $mentor->save();
        return redirect('/mentors');

    }
}
