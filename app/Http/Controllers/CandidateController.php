<?php

namespace App\Http\Controllers;

use App\Models\Candidate;
use Illuminate\Http\Request;

class CandidateController extends Controller
{
    public function store(Request $request){
        // return $request->all();
        $candidate=new Candidate;
        $candidate->name =$request->name;
        $candidate->team_id=$request->team;
        $candidate->save();
        return redirect('/mentors');

    }

}
