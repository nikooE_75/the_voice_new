<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    public function candidate(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Candidate::class);
    }
    public function mentors(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Mentor::class,'activity_mentor','activity_id','mentor_id')->withPivot(['score']);;
    }
    protected $table = 'activities';
    protected $appends = array('average');

//    protected $attributes = ['average' => 0];
    public function getAverageAttribute() {
        $sum=0;
        $count=count($this->mentors);
        $average1=0;
        foreach($this->mentors as $mentor) {
            $sum = $sum + $mentor->pivot->score;
        }
        if($count!=0){
            $average1=$sum/$count;
        }



        return $average1;
    }
    use HasFactory;
}
