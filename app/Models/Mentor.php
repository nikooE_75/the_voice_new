<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mentor extends Model
{
    public function teams(): \Illuminate\Database\Eloquent\Relations\HasMany
    {

        return $this->hasMany(Team::class);
    }
    public function path(){
        return 'mentors/' . $this->id;
    }
    public function activities(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Activity::class,'activity_mentor','mentor_id','activity_id')->withPivot(['score']);
    }
    use HasFactory;
}
