@extends('layout')
@section('content')
    <h2 class="text-center text-success mt-3 shadow p-2 rounded">The Voice</h2>
    <div class="row px-5 mx-5">
        <div class="col-lg-12 text-center p-3 m-3 shadow-lg rounded">
            <h3>
                {{$mentor->name}}
            </h3>
        </div>

        <h4 class="text-center col-lg-12"> His/Her Team's Name </h4>
    @foreach($mentor->teams as $team)

            <div class="col-lg-4  p-3 mx-3 shadow-lg rounded">
                      <h5 class=" text-center text-success">    {{$team->name}} </h5>
                <ul >
                    @foreach($team->candidates as $candidate)
                    <li class="shadow rounded p-2 mt-3">

                            <h6>{{$candidate->name}}</h6>
                        <ul>
                            @foreach($candidate->activities as $activity)
{{--                                @dd($candidate->activities)--}}
                                @if($activity)
                                <li>
                                    <a href="/activity/{{$activity->id}}">
                                        {{$activity->song_name}}
                                    </a>
                                    @if($activity->average)
                                        --Average:
                                        {{$activity->average}}
                                    @endif
                                    @foreach($activity->mentors as $mentor1)
{{--                                        @dd($mentor1)--}}


                                        @if($mentor->name == $mentor1->name)
                                            --Your Score:
                                            {{$mentor1->pivot->score}}
                                        @endif
                                    @endforeach

                                </li>
                                @endif
                            @endforeach
                        </ul>

                    </li>
                    @endforeach
                </ul>

            </div>
                @endforeach

{{--            <ul>--}}
{{--                @foreach($mentor->activities as $activity)--}}
{{--                    <li>--}}


{{--                        {{$activity->pivot->score}}--}}

{{--                    </li>--}}
{{--                @endforeach--}}
{{--            </ul>--}}

    </div>


@stop
