@extends('layout')
@section('content')
    <h2 class="text-center text-success mt-3 shadow p-2 rounded">The Voice</h2>
    @foreach(array_chunk($mentors->getCollection()->all() ,10) as $mentors)
        <h3 class="text-center mt-4">list of Mentors</h3>
        <div class="row px-5 m-4">
            @foreach($mentors as $mentor)
                <div class="col-lg-12 text-center p-3 mx-4 my-2 shadow-lg rounded">
                    <a href="{{$mentor->path()}}">
                        {{$mentor->name}}
                    </a>
                </div>
            @endforeach



    @endforeach
                <div class="col-lg-3 mt-4 mb-5">
                    <h3 class="text-center">Add Mentor</h3>
                    <form class="shadow p-3 h-100 rounded" method="POST" action="/mentor/add">
                        @csrf
                        <input placeholder="Mentor Name" class="shadow rounded my-3 d-block p-2 w-100" name="name" required id="name" >
                        <button type="submit" class="shadow bg-success text-white rounded my-2 d-block p-2 w-100">Add Mentor</button>
                    </form>
                </div>
                <div class="col-lg-3 mt-4 mb-5">
                    <h3 class="text-center">Add Team</h3>
                    <form class="shadow h-100 p-3 rounded" method="POST" action="/team/add">
                        @csrf
                        <input placeholder="Team Name" class="shadow rounded my-3 d-block p-2 w-100" name="name" required id="name" >
                        <select class="shadow border-0 w-100 p-2 text-muted rounded" name="mentor" title="mentor">
                            <option selected value=""> Mentor Name  </option>
                            @foreach(\App\Models\Mentor::all() as $mentor)
                                <option value="{{$mentor->id}}">{{ $mentor->name }}  </option>
                            @endforeach

                        </select>
                        <button type="submit" class="shadow bg-success text-white rounded my-2 d-block p-2 w-100">Add Team</button>
                    </form>
                </div>
                <div class="col-lg-3 mt-4 mb-5">
                    <h3 class="text-center">Add Candidate</h3>
                    <form class="shadow h-100 p-3 rounded" method="POST" action="/candidate/add">
                        @csrf
                        <input placeholder="Candidate Name" class="shadow rounded my-3 d-block p-2 w-100" name="name" required id="name" >
                        <select class="shadow border-0 w-100 p-2 text-muted rounded" name="team" title="team">
                            <option selected value=""> Team Name  </option>
                            @foreach(\App\Models\Team::all() as $team)
                                <option  value="{{$team->id}}"> {{ $team->name }}  </option>
                            @endforeach

                        </select>
                        <button type="submit" class="shadow bg-success text-white rounded my-2 d-block p-2 w-100">Add Candidate</button>
                    </form>
                </div>
                <div class="col-lg-3 mt-4 mb-5">
                    <h3 class="text-center">Add Activity</h3>
                    <form class="shadow h-100 p-3 rounded" method="POST" action="/activity/add">
                        @csrf
                        <input placeholder="Song Name" class="shadow rounded my-3 d-block p-2 w-100" name="name" required id="name" >
                        <input placeholder="Date" type="date" class="shadow rounded my-3 d-block p-2 w-100" name="date" required id="date" >
                        <select class="shadow border-0 w-100 p-2 text-muted rounded" name="candidate" title="candidate">
                            <option selected value=""> Candidate Name  </option>
                            @foreach(\App\Models\Candidate::all() as $candidate)
                                <option  value="{{ $candidate->id}}"> {{  $candidate->name }}  </option>
                            @endforeach

                        </select>
                        <button type="submit" class="shadow bg-success text-white rounded my-2 d-block p-2 w-100">Add Candidate</button>
                    </form>
                </div>
        </div>
{{--    <div>--}}
{{--        {{$mentors->appends(Request::input())->links()}}--}}
{{--    </div>--}}

@stop
