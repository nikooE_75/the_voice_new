@extends('layout')
@section('content')
    <h2 class="text-center text-success mt-3 shadow p-2 rounded">The Voice</h2>
    <div class="row px-5 mx-5">
      <div class="col-lg-12 text-center p-3 m-3 shadow-lg rounded">
          <h3>
             Song Name :   {{$activity->song_name}}
          </h3>
          Date : {{$activity->date}}
      </div>

                @foreach($activity->mentors as $mentor)
            <div class="col-lg-12 text-center p-3 m-4 shadow-lg rounded @if($mentor->pivot->score > 50) border border-success text-success @else border border-danger text-danger @endif">
{{--                        <a href="/note/{{$team->id}}/edit">--}}
                        {{$mentor->name}} :
                            {{$mentor->pivot->score}}
{{--                        </a>--}}
            </div>

                @endforeach
        <div class="col-lg-12 text-center p-3 m-4 shadow-lg rounded @if($activity->average > 50) bg-success text-white @else bg-danger text-white @endif">
            <h4>
           Average :  {{$activity->average}}
            </h4>
        </div>
{{--                    {{$activity->mentors()->attach(5, array('score' => 30))}}--}}

    </div>

@stop
